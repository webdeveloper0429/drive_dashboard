import React from 'react';
import { connect } from 'react-redux';

import {
  Form,
  FormGroup,
  Navbar
} from '@sketchpixy/rubix';

export default class Search extends React.Component {
  componentDidMount() {
    $(".search_cpt").select2();
  }
	render(){
		return(
          <select className="search_cpt">
            <option value="car1">car 1</option>
            <option value="car2">car 2</option>
            <option value="car3">car 3</option>
            <option value="car4">car 4</option>
            <option value="car5">car 5</option>
          </select>
		)
	}
}