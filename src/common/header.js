import React from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';

import { Link, withRouter } from 'react-router';

import l20n, { Entity } from '@sketchpixy/rubix/lib/L20n';

import {
	Label,
	SidebarBtn,
	Dispatcher,
	NavDropdown,
	NavDropdownHover,
	Navbar,
	Nav,
	NavItem,
	MenuItem,
	Badge,
	Button,
	Icon,
	Grid,
	Row,
	Radio,
	Col,
} from '@sketchpixy/rubix';

import Search from './search.js';

// class Brand extends React.Component {
// 	render() {
// 		return (
// 			<Navbar.Header {...this.props}>
// 				<Navbar.Brand tabIndex='-1'>
// 					<a href='#'>
// 						<img src='/imgs/common/logo.png' alt='rubix' width='111' height='28' />
// 					</a>
// 				</Navbar.Brand>
// 			</Navbar.Header>
// 		);
// 	}
// }

// @withRouter
// class HeaderNavigation extends React.Component {
// 	render() {
// 		return (
// 			<Nav pullRight>
// 				<Nav>
// 					<NavItem className='logout' href='#'>
// 						<Icon bundle='fontello' glyph='off-1' />
// 					</NavItem>
// 				</Nav>
// 			</Nav>
// 		);
// 	}
// }

export default class Header extends React.Component {
	// render() {
	// 	const brandImg = <img src='/imgs/app/logo_white.png' alt='rubix' width='328' height='250' />
	// 	return (
	// 		<div id='navbar' {...this.props}>
	// 			<div className='nav_left'>
	// 				{this.props.children}
	// 			</div>
	// 			<div className='nav_right'>
	// 				<NavDropdown noCaret title={brandImg} id='dropdown-basic'>
	// 					<MenuItem eventKey="1">Action</MenuItem>
	// 					<MenuItem eventKey="2">Another action</MenuItem>
	// 					<MenuItem eventKey="3">Active Item</MenuItem>
	// 					<MenuItem divider />
	// 					<MenuItem eventKey="5">Log out</MenuItem>
	// 				</NavDropdown>
					
	// 			</div>
	// 		</div>
	// 	);
	// }
	render() {
		return (
			<Navbar fluid fixedTop inverse>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="#"><img id="brandImg" src='/imgs/app/logo_white.png' alt='rubix' width='328' height='250' /></a>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
        	<Nav className='custom_nav'>
        		<Search />
        	</Nav>
          <Nav pullRight>
            <NavDropdown noCaret title='Anna Sanchez' id='dropdown-basic'>
							<MenuItem eventKey="1">Action</MenuItem>
							<MenuItem eventKey="2">Another action</MenuItem>
							<MenuItem eventKey="3">Active Item</MenuItem>
							<MenuItem divider />
							<MenuItem eventKey="5">Log out</MenuItem>
						</NavDropdown>
          </Nav>
        </Navbar.Collapse>
	    </Navbar>
		);
	}
}
