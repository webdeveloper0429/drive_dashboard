import React from 'react';
import { connect } from 'react-redux';

// import actions from '../../redux/actions';

import {
  Row,
  Col,
  Grid,
  Panel,
  PanelHeader,
  PanelBody,
  PanelContainer,
  Icon,
  Button,
  SplitButton,
  MenuItem,
  Form,
  FormGroup,
  InputGroup,
  FormControl
} from '@sketchpixy/rubix';

import Header from '../../common/header';


export default class Home extends React.Component {
  
  render() {
    return (
      <div>
        <Header>
          
        </Header>
        <Row>
          <Col sm={8} collapseRight>
            <MapPanel />
          </Col>
          <Col sm={4}>
            <DriverNamePanel />
            <AlertPanel />
            <AlertPanel />
          </Col>
        </Row>
        <Row>
          <Col sm={4} collapseRight>
            <StatusPanel />
          </Col>
          <Col sm={4} collapseRight>
            <DriverNamePanel />
            <JoueryDetailPanel />
          </Col>
          <Col sm={4}>
            <DriverNamePanel />
            <JoueryDetailPanel />
          </Col>
        </Row>
      </div>
    );
  }
}

class MapPanel extends React.Component{
  geoCode(address) {
    GMaps.geocode({
      address: address,
      callback: (results, status) => {
        if (status == 'OK') {
          var latlng = results[0].geometry.location;
          this.geocode.setCenter(latlng.lat(), latlng.lng());
          this.geocode.addMarker({
            lat: latlng.lat(),
            lng: latlng.lng(),
            infoWindow: {
              content: '<div><strong>Address:</strong> '+results[0].formatted_address+'</div>'
            }
          });
        }
      }
    });
  }
  handleSubmit(e) {
    e.preventDefault();
    e.stopPropagation();
    this.geoCode($('#address').val());
  }
  componentDidMount() {
    (() => {
      this.geocode = new GMaps({
        scrollwheel: false,
        div: '#geocode',
        zoom: 16,
        lat: -12.043333,
        lng: -77.028333
      });
      this.geoCode('New York, NY, USA');
    })();
  }
  render() {
    return (
      <PanelContainer>
        <Panel>
          <PanelBody style={{padding: 25}}>
            <Form onSubmit={::this.handleSubmit}>
              <FormGroup>
                <InputGroup>
                  <FormControl type='text' id='address' placeholder='Address' defaultValue='New York, NY, USA' />
                  <InputGroup.Button className='plain'>
                    <Button outlined onlyOnHover type='submit' bsStyle='darkgreen45'>search</Button>
                  </InputGroup.Button>
                </InputGroup>
              </FormGroup>
            </Form>
            <div id="geocode" style={{height: 500}} ></div>
          </PanelBody>
        </Panel>
      </PanelContainer>
    )
  }
}

class StatusPanel extends React.Component{
  render() {
    return (
      <PanelContainer>
        <Panel>
          <PanelHeader className='bg-blue'>
            <Grid>
              <Row>
                <Col xs={12} className='fg-white'>
                  <h3>Status</h3>
                </Col>
              </Row>
            </Grid>
          </PanelHeader>
          <PanelBody >
            <Grid>
              <Row>
                <Col xs={4} className='fg-black75'>
                  <h4>Engine</h4>
                </Col>
                <Col xs={8}>
                  <h4>ON</h4>
                </Col>
              </Row>
              <Row>
                <Col xs={4} className='fg-black75'>
                  <h4>Speed</h4>
                </Col>
                <Col xs={8}>
                  <h4>98Km/h</h4>
                </Col>
              </Row>
              <Row>
                <Col xs={4} className='fg-black75'>
                  <h4>Health</h4>
                </Col>
                <Col xs={8}>
                  <h4></h4>
                </Col>
              </Row>
            </Grid>
          </PanelBody>
        </Panel>
      </PanelContainer>
    )
  }
}

class DriverNamePanel extends React.Component{
  render() {
    return (
      <PanelContainer>
        <Panel>
          <PanelHeader className='bg-darkblue'>
            <Grid>
              <Row>
                <Col xs={12} className='fg-white'>
                  <h3>Driver Name</h3>
                </Col>
              </Row>
            </Grid>
          </PanelHeader>
          <PanelBody >
            <Grid>
              <Row>
                <Col xs={4} className='fg-black75'>
                  <h4>Driver Name</h4>
                </Col>
                <Col xs={8}>
                  <h4>SGX 4553Y</h4>
                </Col>
              </Row>
              <Row>
                <Col xs={4} className='fg-black75'>
                  <h4>Last Seen</h4>
                </Col>
                <Col xs={8}>
                  <h4>1 May 2017 09:35</h4>
                </Col>
              </Row>
              <Row>
                <Col xs={4} className='fg-black75'>
                  <h4>Honda Vezel</h4>
                </Col>
                <Col xs={8}>
                  <h4></h4>
                </Col>
              </Row>
            </Grid>
          </PanelBody>
        </Panel>
      </PanelContainer>
    )
  }
}

class JoueryDetailPanel extends React.Component{
  render() {
    return (
      <PanelContainer>
        <Panel>
          <PanelHeader className='bg-green'>
            <Grid>
              <Row>
                <Col xs={12} className='fg-white'>
                  <h3>Journey Details</h3>
                </Col>
              </Row>
            </Grid>
          </PanelHeader>
          <PanelBody >
            <Grid>
              <Row>
                <Col xs={4} className='fg-black75'>
                  <h4>Journey Distance</h4>
                </Col>
                <Col xs={8}>
                  <h4></h4>
                </Col>
              </Row>
              <Row>
                <Col xs={4} className='fg-black75'>
                  <h4>Journey Duration</h4>
                </Col>
                <Col xs={8}>
                  <h4></h4>
                </Col>
              </Row>
              <Row>
                <Col xs={4} className='fg-black75'>
                  <h4>Average Speed</h4>
                </Col>
                <Col xs={8}>
                  <h4></h4>
                </Col>
              </Row>
            </Grid>
          </PanelBody>
        </Panel>
      </PanelContainer>
    )
  }
}

class AlertPanel extends React.Component{
  render() {
    return (
      <PanelContainer>
        <Panel>
          <PanelHeader className='bg-red'>
            <Grid>
              <Row>
                <Col xs={12} className='fg-white'>
                  <h3>Alerts</h3>
                </Col>
              </Row>
            </Grid>
          </PanelHeader>
          <PanelBody >
            <Grid>
              <Row>
                <Col xs={4} className='fg-black75'>
                  <h4></h4>
                </Col>
                <Col xs={8}>
                  <h4></h4>
                </Col>
              </Row>
            </Grid>
          </PanelBody>
        </Panel>
      </PanelContainer>
    )
  }
}